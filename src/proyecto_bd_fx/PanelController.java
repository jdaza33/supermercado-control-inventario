package proyecto_bd_fx;

import java.awt.Desktop;
import java.io.File;
import proyecto_bd_fx.Almacen2;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.MenuBar;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import static proyecto_bd_fx.Conexion.connect;


public class PanelController implements Initializable {

    @FXML
    private MenuBar menu;
    @FXML
    private ComboBox<?> almacen;
    @FXML
    private AnchorPane ventana_panel;
    @FXML
    TableView<Almacen2> table = new TableView<Almacen2>(); 
    @FXML
    private TableColumn<Almacen2, String> id;
    @FXML
    private TableColumn<Almacen2, String> nombre;
    @FXML
    private TableColumn<Almacen2, String> proveedor;
    @FXML
    private TableColumn<Almacen2, String> cantidad;
    @FXML
    private TableColumn<Almacen2, String> precio;
    @FXML
    private TableColumn<Almacen2, String> fecha;
    @FXML
    private TextField ced;
    @FXML
    private ImageView foto;
    
    final ObservableList<Almacen2> data = FXCollections.observableArrayList();  
    
    Funciones f=new Funciones();
    Conexion c=new Conexion();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       
       id.setCellValueFactory(new PropertyValueFactory<Almacen2, String>("id"));
       nombre.setCellValueFactory(new PropertyValueFactory<Almacen2, String>("nombre"));
       proveedor.setCellValueFactory(new PropertyValueFactory<Almacen2, String>("proveedor"));
       fecha.setCellValueFactory(new PropertyValueFactory<Almacen2, String>("fecha"));
       cantidad.setCellValueFactory(new PropertyValueFactory<Almacen2, String>("cantidad"));
       precio.setCellValueFactory(new PropertyValueFactory<Almacen2, String>("precio"));
       
       table.setItems(data);
       
       fotis();
    }
    
    @FXML
    private void ingresar() throws IOException{
        
        f.intercambio("Ingresar.fxml",ventana_panel);

    }
    
    @FXML
    private void papelera() throws IOException{
        
        f.intercambio("Papelera.fxml",ventana_panel);

    }
    
    @FXML
    private void registros() throws IOException{
        
        f.intercambio("Registros.fxml",ventana_panel);

    }
    
    @FXML
    private void clientes() throws IOException{
        
        f.intercambio("Clientes.fxml",ventana_panel);

    }
    
    @FXML
    private void mostrarClientes() throws IOException{
        
        f.intercambio("MostrarCliente.fxml",ventana_panel);

    }
    @FXML
    private void empresa() throws IOException{
        
        f.intercambio("Empresa.fxml",ventana_panel);

    }
    
    
    @FXML
    private void modificar() throws IOException{
        

        JPasswordField pf = new JPasswordField();
        int okCxl = JOptionPane.showConfirmDialog(null, pf, "Clave Administrador", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        String aux = new String(pf.getPassword());
        
        boolean op=f.solicitarClave(aux);
        
        if(op==true){
            f.intercambio("Modificar.fxml",ventana_panel);
        }else if(op==false){
            JOptionPane.showMessageDialog(null, "Clave Incorrecta");
        }
    }
    
    @FXML
    private void eliminar() throws IOException{
        

        JPasswordField pf = new JPasswordField();
        int okCxl = JOptionPane.showConfirmDialog(null, pf, "Clave Administrador", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        String aux = new String(pf.getPassword());
        
        boolean op=f.solicitarClave(aux);
        
        if(op==true){
            f.intercambio("Eliminar.fxml",ventana_panel);
        }else if(op==false){
            JOptionPane.showMessageDialog(null, "Clave Incorrecta");
        }
    }
    
    @FXML
    void actualizar(){
        
        table.getItems().clear();
        
        String almacen_destino,almacenn;
        almacenn=(String) almacen.getValue();
        almacen_destino=f.buscarAlmacen(almacenn);
        
        c.connect();
        
        String nombre, proveedor, fecha;
        int idd, cant;
        double precio;
    
    ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from "+almacen_destino);
            result = st.executeQuery();
            while (result.next()) {
                idd=result.getInt("id");
                nombre=result.getString("nombre");
                proveedor=result.getString("proveedor");
                fecha=result.getString("ultima_entrada");
                cant=result.getInt("cantidad");
                precio=result.getDouble("precio");
                data.add(new Almacen2(nombre, proveedor, Integer.toString(cant), Double.toString(precio), fecha, Integer.toString(idd)));
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
        c.close();
    }
    
    @FXML
    private int insertarFoto(){
        
        c.connect();
        String usuario=c.leerAuxiliar();
        c.close();
        
        ced.setText(usuario);
        
        String aux;
        int idd=0;
        
    c.connect();
    ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from empleados_datos");
            result = st.executeQuery();
            while (result.next()) {
                
                aux=result.getString("cedula");
                boolean op=aux.equals(usuario);
                if(op==true){
                    idd=result.getInt("id");
                }
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
        c.close();
        
        
        return idd;
    }
    
    @FXML
    private void fotis(){
        
        int idd=insertarFoto();
        int aux_int;
        String ruta=null;
        
        c.connect();
        ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from foto_empleado");
            result = st.executeQuery();
            while (result.next()) {
                
                aux_int=result.getInt("id");
                if(aux_int==idd){
                   ruta=result.getString("ruta");
                }
                
                
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
        c.close();
        
        File imgFile=new File(ruta);
        
        if (imgFile != null) {
            Image image = new Image("file:" + imgFile.getAbsolutePath());
            foto.setImage(image);
        }
    }
    
    @FXML
    private void abrirPDF(){
        try {
     File path = new File ("src\\fotos_empleados\\manual.pdf");
     Desktop.getDesktop().open(path);
    }catch (IOException ex) {
     ex.printStackTrace();
}
    }
    
    @FXML
    private void guardarA(){
        f.guardarArchivo();
    }
    
    
    }
    
   
    
