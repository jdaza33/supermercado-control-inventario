package proyecto_bd_fx;

import java.util.*;
import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Conexion {
    static String url = "bd/base.db";
    static Connection connect;
    
public static void connect(){
 try {
     connect = DriverManager.getConnection("jdbc:sqlite:"+url);
     if (connect!=null) {
         System.out.println("Conectado");
     }
 }catch (SQLException ex) {
     System.err.println("No se ha podido conectar a la base de datos\n"+ex.getMessage());
 }
}

public static void close(){
        try {
            connect.close();
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
 }

public void mostrarAlumnos(){
        ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from login");
            result = st.executeQuery();
            while (result.next()) {
                System.out.print("ID: ");
                System.out.println(result.getInt("id"));
 
                System.out.print("Cedula: ");
                System.out.println(result.getString("cedula"));
 
                System.out.print("Clave: ");
                System.out.println(result.getString("clave"));
 
                System.out.println("=======================");
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
    }

public static int login(String cedula, String clave){
    
    int res=0;
    
    ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from login");
            result = st.executeQuery();
            while (result.next()) {
                
                String usuario_aux=result.getString("cedula");
                String clave_aux=result.getString("clave");
                
                boolean usuario_value=cedula.equals(usuario_aux);
                boolean clave_value=clave.equals(clave_aux);
                
                if((usuario_value==true) && (clave_value==true)){
                        res=1;
                }
                if((usuario_value==true) && (clave_value==false)){
                        res=2;
                }
 
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
    
    return res;
}

public String buscarCedula(int id){
    
    String usuario=null;
    
    ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from empleados_datos");
            result = st.executeQuery();
            while (result.next()) {
                
                int id_aux=result.getInt("id");
               
                if(id_aux==id){
                        usuario=result.getString("cedula");
                }else{
                    usuario=null;
                }
 
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
    
    return usuario;
}

public String buscarClave(int id){
    
    String clave=null;
    
    ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from contraseña");
            result = st.executeQuery();
            while (result.next()) {
                
                int id_aux=result.getInt("id");
               
                if(id_aux==id){
                        clave=result.getString("clave");
                }else{
                    clave=null;
                }
 
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
    
    return clave;
}

public void registrarLogin(int id){
    
    Statement stmt = null;
    Connection c = null;
    
    String cedula=buscarCedula(id);
    String clave=buscarClave(id);
    
    try{
    c = DriverManager.getConnection("jdbc:sqlite:"+url);
    c.setAutoCommit(false);
    stmt = c.createStatement();
    String sql = "INSERT INTO login (id, cedula, clave)" 
            + "VALUES ("+id+", '"+cedula+"', '"+clave+"')";
    stmt.executeUpdate(sql);
    stmt.close();
    c.commit();
    c.close();
    }catch ( Exception e ){
      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
      System.exit(0);
    }
    
}

public int sumarId(){
    
    int id=0;
    ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from empleados_datos");
            result = st.executeQuery();
            while (result.next()) {
                id=result.getInt("id");
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
        id=id+1;
    //System.out.println(id);
    return id;
    
}

public void registrar1(int id, String pn, String sn, String pa, String sa, int cedula, int rif, String sexo, String nacimiento, String correo, String celular, String telefono, int id_ps, int id_d, int id_te, int id_foto, int id_login, String nacionalidad) throws SQLException{
    
    Statement stmt = null;
    Connection c = null;
    
    try{
    c = DriverManager.getConnection("jdbc:sqlite:"+url);
    c.setAutoCommit(false);
    stmt = c.createStatement();
    String sql = "INSERT INTO empleados_datos (id, primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, cedula, rif, sexo, nacimiento, correo, celular, telefono, id_pregunta_secreta, id_direccion, id_tipo_empleado, id_foto, id_login, nacionalidad)" 
            + "VALUES ("+id+", '"+pn+"', '"+sn+"', '"+pa+"', '"+sa+"', "+cedula+", "+rif+", '"+sexo+"', '"+nacimiento+"', '"+correo+"', '"+celular+"', '"+telefono+"', "+id_ps+", "+id_d+", "+id_te+", "+id_foto+", "+id_login+", '"+nacionalidad+"')";
    stmt.executeUpdate(sql);
    stmt.close();
    c.commit();
    c.close();
    }catch ( Exception e ){
      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
      System.exit(0);
    }
    
}

public void registrar2(int id, String es, String ci, String mu, String pa, String av, String se, String re, String nr, String pais){
    
    Statement stmt = null;
    Connection c = null;
    
    try{
    c = DriverManager.getConnection("jdbc:sqlite:"+url);
    c.setAutoCommit(false);
    stmt = c.createStatement();
    String sql = "INSERT INTO direccion_empleado (id, estado, ciudad, municipio, parroquia, av_calle, sector, residencia_urb, nro_casa_apt, pais)" 
            + "VALUES ("+id+", '"+es+"', '"+ci+"', '"+mu+"', '"+pa+"', '"+av+"', '"+se+"', '"+re+"', '"+nr+"', '"+pais+"')";
    stmt.executeUpdate(sql);
    stmt.close();
    c.commit();
    c.close();
    }catch ( Exception e ){
      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
      System.exit(0);
    }
    
}

public void registrarR3(int id, String r, String p){
    
    Statement stmt = null;
    Connection c = null;
    
    try{
    c = DriverManager.getConnection("jdbc:sqlite:"+url);
    c.setAutoCommit(false);
    stmt = c.createStatement();
    String sql = "INSERT INTO pregunta_secreta (id, respuesta, pregunta)" 
            + "VALUES ("+id+", '"+r+"', '"+p+"')";
    stmt.executeUpdate(sql);
    stmt.close();
    c.commit();
    c.close();
    }catch ( Exception e ){
      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
    }
    
}

public void registrarC3(int id, String cl){
    
    Statement stmt = null;
    Connection c = null;
    
    try{
    c = DriverManager.getConnection("jdbc:sqlite:"+url);
    c.setAutoCommit(false);
    stmt = c.createStatement();
    String sql = "INSERT INTO contraseña (id, clave)" 
            + "VALUES ("+id+", '"+cl+"')";
    stmt.executeUpdate(sql);
    stmt.close();
    c.commit();
    c.close();
    }catch ( Exception e ){
      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
    }
    
}

public void registrarFoto(int id, String ruta){
    
    Statement stmt = null;
    Connection c = null;
    
    try{
    c = DriverManager.getConnection("jdbc:sqlite:"+url);
    c.setAutoCommit(false);
    stmt = c.createStatement();
    String sql = "INSERT INTO foto_empleado (id, ruta)" 
            + "VALUES ("+id+", '"+ruta+"')";
    stmt.executeUpdate(sql);
    stmt.close();
    c.commit();
    c.close();
    }catch ( Exception e ){
      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
    }
    
}

public void registrar4(int id, String cargo, String estado){
    
    Statement stmt = null;
    Connection c = null;
    
    try{
    c = DriverManager.getConnection("jdbc:sqlite:"+url);
    c.setAutoCommit(false);
    stmt = c.createStatement();
    String sql = "INSERT INTO tipo_cargo_empleado (id, cargo, estado)" 
            + "VALUES ("+id+", '"+cargo+"', '"+estado+"')";
    stmt.executeUpdate(sql);
    stmt.close();
    c.commit();
    c.close();
    }catch ( Exception e ){
      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
    }
    
}

public void registrarA(int id, String nombre, String proveedor, String fecha, int cant, double precio, String almacen){
    
    Statement stmt = null;
    Connection c = null;
    
    try{
    c = DriverManager.getConnection("jdbc:sqlite:"+url);
    c.setAutoCommit(false);
    stmt = c.createStatement();
    String sql = "INSERT INTO '"+almacen+"' (id, nombre, proveedor, ultima_entrada, cantidad, precio)" 
            + "VALUES ("+id+", '"+nombre+"', '"+proveedor+"', '"+fecha+"', "+cant+", "+precio+")";
    stmt.executeUpdate(sql);
    stmt.close();
    c.commit();
    c.close();
    }catch ( Exception e ){
      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
    }
    
}

public int idAlmacen(String almacen){
    
    int id=0;
    ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from "+almacen);
            result = st.executeQuery();
            while (result.next()) {
                id=result.getInt("id");
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
        id=id+1;
    //System.out.println(id);
    return id;
    
}

    public void modificarA(String almacen, int id, String column, String dato){

    Statement stmt = null;
    
    boolean i=column.equals("cantidad");
    boolean d=column.equals("precio");
   
    
    try {
      stmt = connect.createStatement();
      if(i==true){
          int aux=Integer.parseInt(dato);
          String sql = "UPDATE '"+almacen+"' set '"+column+"' = "+aux+" where id="+id+"";
          stmt.executeUpdate(sql);
          stmt.close();
          connect.close(); 
      }else if(d==true){
          double aux=Double.parseDouble(dato);
          String sql = "UPDATE '"+almacen+"' set '"+column+"' = "+aux+" where id="+id+"";
          stmt.executeUpdate(sql);
          stmt.close();
          connect.close(); 
      }else{
          String sql = "UPDATE '"+almacen+"' set '"+column+"' = '"+dato+"' where id="+id+"";
          stmt.executeUpdate(sql);
          stmt.close();
          connect.close(); 
      }   
    } catch ( Exception e ) {
      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
    }
    }
    
    public int buscarAdmin(String clave){
        
        int id_destino=0;
        boolean op;
        String clave_aux;
        
        ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from contraseña");
            result = st.executeQuery();
            while (result.next()) {
                clave_aux=result.getString("clave");
                op=clave.equals(clave_aux);
                if(op==true){
                   id_destino=result.getInt("id");
                }
                
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
        
        return id_destino;
    }
    
    public boolean buscarAdmin2(int id){
        
        int id_aux;
        boolean op=false;
        boolean aux=false;
        String cargo, cargo_aux;
        cargo="Administrativo";
        
        ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from tipo_cargo_empleado");
            result = st.executeQuery();
            while (result.next()) {
                id_aux=result.getInt("id");
                if(id_aux==id){
                    cargo_aux=result.getString("cargo");
                    aux=cargo.equals(cargo_aux);
                    if(aux==true){
                        op=true;
                    }
                }
                
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
        
        return op;
    }
    
    public void eliminarA(String tabla, int id){
        
    Statement stmt = null;
    try {
      connect.setAutoCommit(false);
      stmt = connect.createStatement();
      String sql = "DELETE from '"+tabla+"' where id="+id+";";
      stmt.executeUpdate(sql);
      connect.commit();
      stmt.close();
      connect.close();
    } catch ( Exception e ) {
      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
    }
        
    }
    
    public void auxiliar(String cedula){
        Statement stmt = null;
        Connection c = null;
        int id=1;
        try{
        c = DriverManager.getConnection("jdbc:sqlite:"+url);
        c.setAutoCommit(false);
        stmt = c.createStatement();
        String sql = "INSERT INTO auxiliar (id, cedula)" 
            + "VALUES ("+id+", '"+cedula+"')";
        stmt.executeUpdate(sql);
        stmt.close();
        c.commit();
        c.close();
        }catch ( Exception e ){
          System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
    }
    
    public void eliminarAuxiliar(){
        
    Statement stmt = null;
    try {
      connect.setAutoCommit(false);
      stmt = connect.createStatement();
      String sql = "DELETE from auxiliar where id=1;";
      stmt.executeUpdate(sql);
      connect.commit();
      stmt.close();
      connect.close();
    } catch ( Exception e ) {
      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
    }
        
    }
    
public void registros(String user, String hacer, String fecha){
    
        Statement stmt = null;
        Connection c = null;
        try{
        c = DriverManager.getConnection("jdbc:sqlite:"+url);
        c.setAutoCommit(false);
        stmt = c.createStatement();
        String sql = "INSERT INTO registros (usuario,hacer,fecha)" 
            + "VALUES ('"+user+"', '"+hacer+"', '"+fecha+"')";
        stmt.executeUpdate(sql);
        stmt.close();
        c.commit();
        c.close();
        }catch ( Exception e ){
          System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
    
}

public String leerAuxiliar(){
       
        String cedula=null;

        ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from auxiliar");
            result = st.executeQuery();
            while (result.next()) {
                cedula=result.getString("cedula");
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
    
    return cedula;
}

public void ingresarClientes(int id, String nombre, String ced){
    
        Statement stmt = null;
        Connection c = null;
        try{
        c = DriverManager.getConnection("jdbc:sqlite:"+url);
        c.setAutoCommit(false);
        stmt = c.createStatement();
        String sql = "INSERT INTO clientes (id, nombre, id_direccion, cedula_rif)" 
            + "VALUES ("+id+", '"+nombre+"', "+id+", '"+ced+"')";
        stmt.executeUpdate(sql);
        stmt.close();
        c.commit();
        c.close();
        }catch ( Exception e ){
          System.err.println( e.getClass().getName() + ": " + e.getMessage() );
        }
    
}

public void direccionClientes(int id, String estado, String ciudad, String municipio, String parroquia, String av, String res, String casa, String pais, String sec){
    
    Statement stmt = null;
        Connection c = null;
        try{
        c = DriverManager.getConnection("jdbc:sqlite:"+url);
        c.setAutoCommit(false);
        stmt = c.createStatement();
        String sql = "INSERT INTO direccion_clientes (id, estado, ciudad, municipio, parroquia, av_calle, sector, residencia_urb, nro_casa_apt, pais)" 
            + "VALUES ("+id+", '"+estado+"', '"+ciudad+"', '"+municipio+"', '"+parroquia+"', '"+av+"', '"+sec+"', '"+res+"', '"+casa+"', '"+pais+"')";
        stmt.executeUpdate(sql);
        stmt.close();
        c.commit();
        c.close();
        }catch ( Exception e ){
          System.err.println( e.getClass().getName() + ": " + e.getMessage() );
          System.exit(0);
        }
    
    
}
    

}
