package proyecto_bd_fx;

import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javax.swing.JOptionPane;
import static proyecto_bd_fx.Conexion.connect;


public class EliminarController implements Initializable {

    @FXML
    private TextField id;
    @FXML
    private Font x2;
    @FXML
    private TextField nombre;
    @FXML
    private Insets x1;
    @FXML
    private TextField proveedor;
    @FXML
    private TextField cantidad;
    @FXML
    private TextField precio;
    @FXML
    private TextField fecha;
    @FXML
    private ComboBox<?> almacen;
    @FXML
    private AnchorPane ventana_eliminar;

    Funciones f=new Funciones();
    Conexion c=new Conexion();
    String fecha_;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        fecha_=String.valueOf(dateFormat.format(date));
        
    }    
    
    @FXML
    private void reset(){
        
        nombre.setText("");
        proveedor.setText("");
        fecha.setText("");
        cantidad.setText("");
        precio.setText("");
        id.setText("");
        
        nombre.setEditable(false);
        proveedor.setEditable(false);
        fecha.setEditable(false);
        cantidad.setEditable(false);
        precio.setEditable(false);
        id.setEditable(true);
        
    }
    
    @FXML
    private void buscarId(){
        
        String almacenn=(String) almacen.getValue();
        String almacen_destino=f.buscarAlmacen(almacenn);
        
        String idd=id.getText();
        int id_int=Integer.parseInt(idd);
        
        int id_aux, cant;
        String nombree, proveedorr, fechaa;
        double precioo;
        c.connect();
        ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from "+almacen_destino);
            result = st.executeQuery();
            while (result.next()) {
                id_aux=result.getInt("id");
                if(id_int==id_aux){
                    id.setText(Integer.toString(id_int));
                    nombree=result.getString("nombre");
                    nombre.setText(nombree);
                    proveedorr=result.getString("proveedor");
                    proveedor.setText(proveedorr);
                    fechaa=result.getString("ultima_entrada");
                    fecha.setText(fechaa);
                    cant=result.getInt("cantidad");
                    cantidad.setText(Integer.toString(cant));
                    precioo=result.getDouble("precio");
                    precio.setText(Double.toString(precioo));
                }
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
        c.close();
        
        nombre.setEditable(false);
        proveedor.setEditable(false);
        fecha.setEditable(false);
        cantidad.setEditable(false);
        precio.setEditable(false);
        id.setEditable(false);
        
    }
    
    @FXML
    private void eliminar() throws IOException{
       
        String almacenn=(String) almacen.getValue();
        String almacen_destino=f.buscarAlmacen(almacenn);
        
        String idd=id.getText();
        int id_int=Integer.parseInt(idd);
        
        String nombree, proveedorr, fechaa, cant, precioo;
        int cant_int;
        double precioo_double;
        
        nombree=nombre.getText();
        proveedorr=proveedor.getText();
        fechaa=fecha.getText();
        cant=cantidad.getText();
        cant_int=Integer.parseInt(cant);
        precioo=precio.getText();
        precioo_double=Double.parseDouble(precioo);
        
        
        c.connect();
        c.eliminarA(almacen_destino, id_int);
        c.close();
        
        c.connect();
        c.registrarA(id_int, nombree, proveedorr, fechaa, cant_int, precioo_double, "papelera");
        c.close();
        
        JOptionPane.showMessageDialog(null, "Producto Enviado a Papelera");
        f.intercambio("Panel.fxml", ventana_eliminar);
        
            c.connect();
            String usuario=c.leerAuxiliar();
            c.close();
            
            c.connect();
            c.registros(usuario,"Eliminar Producto",fecha_);
            c.close();
       
    }
    
    @FXML
    private void cancelar() throws IOException{
        f.intercambio("Panel.fxml", ventana_eliminar);
    }
    
    
}
