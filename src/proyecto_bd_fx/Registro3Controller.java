package proyecto_bd_fx;

import java.io.IOException;
import java.net.URL;
import java.util.Objects;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javax.swing.JOptionPane;


public class Registro3Controller implements Initializable {

    @FXML
    private AnchorPane ventana_registro3;
    @FXML
    private TextField ps;
    @FXML
    private PasswordField rs;
    @FXML
    private PasswordField rs2;
    @FXML
    private PasswordField clave;
    @FXML
    private PasswordField clave2;

    Funciones f=new Funciones();
    Conexion c=new Conexion();
    String p=f.preguntaSecreta();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ps.setText(p);
    }    
    
    @FXML
    private void siguiente() throws IOException{
        f.intercambio("Registro4.fxml", ventana_registro3);
    }
    
    @FXML
    private void cancelar() throws IOException{
        f.intercambio("login.fxml", ventana_registro3);
    }
    
    @FXML
    private String mostrarPs(){
        String pss;
        pss=f.preguntaSecreta();
        return pss;
    }
    
    @FXML
    private void seguridadE() throws IOException{
        String r1, r2, c1, c2;
        int idd;
        
        c.connect();
        idd=c.sumarId();
        idd=idd-1;
        c.close();
        
        r1=rs.getText();
        r2=rs2.getText();
        c1=clave.getText();
        c2=clave2.getText();
        
        if((Objects.equals(r1, r2))==true){
            if((Objects.equals(c1, c2))==true){
                String rss, clave;
                rss=f.getMD5(r1);
                clave=f.getMD5(c1);
                
                c.connect();
                c.registrarR3(idd, rss,p);
                c.registrarC3(idd,clave);
                c.close();
                siguiente();
            }
        }
        else{
            JOptionPane.showMessageDialog(null, "Respuesta o Clave no coinciden");
        }
        
    }
    
}
