
package proyecto_bd_fx;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import static proyecto_bd_fx.Conexion.connect;


public class EmpresaController implements Initializable {

    @FXML
    private ListView<String> lista;
    @FXML
    private ImageView logo;
    @FXML
    private AnchorPane ventana_empresa;

    final ObservableList<String> data = FXCollections.observableArrayList(); 
    
    Funciones f=new Funciones();
    Conexion c=new Conexion();
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        lista.setItems(data);
        leer();
        insertarImg();
    }    
    
    @FXML
    private void leer(){
        
        String rif, creacion, dueño, subdueño, tipo,lema;
        int id;
        c.connect();
        ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from datos_empresa");
            result = st.executeQuery();
            while (result.next()) {
                rif=result.getString("rif");
                creacion=result.getString("creacion");
                dueño=result.getString("dueño");
                subdueño=result.getString("subdueño");
                tipo=result.getString("tipo_empresa");
                lema=result.getString("lema");
                data.add(new String("Creacion: "+creacion));
                data.add(new String("Dueño: "+dueño));
                data.add(new String("Sub Dueño: "+subdueño));
                data.add(new String("RIF: "+rif));
                data.add(new String("Tipo: "+tipo));
                data.add(new String("Lema: "+lema));
                
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
        c.close();
    }
    
    @FXML
    private void volver() throws IOException{
        f.intercambio("Panel.fxml", ventana_empresa);
    }
    
    @FXML
    private void insertarImg(){
        
        String ruta=null;
        int id;
        c.connect();
        ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from logo_empresa");
            result = st.executeQuery();
            while (result.next()) {
                ruta=result.getString("ruta");
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
        c.close();
        
        File imgFile=new File(ruta);
        
        if (imgFile != null) {
            Image image = new Image("file:" + imgFile.getAbsolutePath());
            logo.setImage(image);
        }
        
    }
    
}
