
package proyecto_bd_fx;

import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javax.swing.JOptionPane;


public class Direccion_clientesController implements Initializable {

    @FXML
    private TextField estado;
    @FXML
    private Insets x1;
    @FXML
    private TextField municipio;
    @FXML
    private TextField av_calle;
    @FXML
    private TextField res_urb;
    @FXML
    private TextField ciudad;
    @FXML
    private TextField parroquia;
    @FXML
    private TextField sector;
    @FXML
    private TextField casa_apt;
    @FXML
    private TextField pais;
    @FXML
    private AnchorPane ventana_direccionc;

    Funciones f=new Funciones();
    Conexion c=new Conexion();
    String fecha_;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        fecha_=String.valueOf(dateFormat.format(date));
        
    }    
    
    @FXML
    private void finalizar() throws IOException{
        f.intercambio("Panel.fxml",ventana_direccionc);
    }
    
    @FXML
    private void cancelar() throws IOException{
        f.intercambio("Panel.fxml",ventana_direccionc);
    }
    
    @FXML
    private void ingresar() throws IOException{
        
        
        String estadoo, ciudadd, municipioo, parroquiaa, paiss, av, casa, sectorr, res;
        
        c.connect();
        int id_int=c.idAlmacen("clientes");
        int idd=id_int -1;
        c.close();
        
        estadoo=estado.getText();
        ciudadd=ciudad.getText();
        municipioo=municipio.getText();
        parroquiaa=parroquia.getText();
        paiss=pais.getText();
        av=av_calle.getText();
        sectorr=sector.getText();
        res=res_urb.getText();
        casa=casa_apt.getText();
        
        c.connect();
        c.direccionClientes(idd, estadoo, ciudadd, municipioo, parroquiaa, av, res,  casa,  paiss, sectorr);
        c.close();
        
        JOptionPane.showMessageDialog(null, "Cliente ingresado con exito");
        finalizar(); 
        
            c.connect();
            String usuario=c.leerAuxiliar();
            c.close();
            
            c.connect();
            c.registros(usuario,"Registro Cliente",fecha_);
            c.close();
    }
    
}
