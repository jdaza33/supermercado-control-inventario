package proyecto_bd_fx;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;

public class SubPanelController implements Initializable {

    @FXML
    private AnchorPane ventana_subpanel;

    Funciones f=new Funciones();
    Conexion c=new Conexion();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    
    
    @FXML
    private void perfil() throws IOException{
        f.intercambio("Perfil.fxml",ventana_subpanel);  
    }
    
    @FXML
    private void almacen() throws IOException{
        f.intercambio("Panel.fxml",ventana_subpanel);  
    }
    
}
