package proyecto_bd_fx;

import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import static proyecto_bd_fx.Conexion.connect;


public class PapeleraController implements Initializable {

    @FXML
    private AnchorPane ventana_papelera;
    @FXML
    private ListView<String> lista;
    
    final ObservableList<String> data = FXCollections.observableArrayList();  

    Funciones f=new Funciones();
    Conexion c=new Conexion();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        lista.setItems(data);
        leer();
    }    
    
    @FXML
    private void leer(){
        
        String nombre, proveedor, fecha;
        int id, cant;
        double precio;
        c.connect();
        ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from papelera");
            result = st.executeQuery();
            while (result.next()) {
                id=result.getInt("id");
                nombre=result.getString("nombre");
                proveedor=result.getString("proveedor");
                fecha=result.getString("ultima_entrada");
                cant=result.getInt("cantidad");
                precio=result.getDouble("precio");
                data.add(new String(id+"|"+nombre+"|"+proveedor+"|"+fecha+"|"+cant+"|"+precio));
                
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
        c.close();
    }
    
    @FXML
    private void volver() throws IOException{
        f.intercambio("Panel.fxml", ventana_papelera);
    }
    
}
