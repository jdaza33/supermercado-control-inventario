package proyecto_bd_fx;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.util.StringTokenizer;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;


public class Funciones {
    
    public void intercambio(String abrir, AnchorPane cerrar) throws IOException{
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(abrir));
           Parent root1 = (Parent) fxmlLoader.load();
           Stage stage = new Stage();
           //stage.initModality(Modality.APPLICATION_MODAL);
           //stage.initStyle(StageStyle.UNDECORATED);
           stage.setScene(new Scene(root1));  
           stage.show();
           
           Stage stagee = (Stage) cerrar.getScene().getWindow();
           //stage.initModality(Modality.APPLICATION_MODAL);
           //stage.initStyle(StageStyle.UNDECORATED);
           stagee.close();
    }
    
    public String preguntaSecreta(){
        String[] ps={"¿Pelicula Favorita?", "¿Idolo Favorito?", "¿Nombre Mascota?", "¿Fruta Preferida?", "¿Comida que mas te gusta?"};
        int nro = (int) (Math.random() * 4);
        String aux=ps[nro];
        return aux;
    }
    
    public static String getMD5(String input) {
        byte[] source;
        try {
            //Get byte according by specified coding.
            source = input.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            source = input.getBytes();
        }
        String result = null;
        char hexDigits[] = {'0', '1', '2', '3', '4', '5', '6', '7',
                '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(source);
            //The result should be one 128 integer
            byte temp[] = md.digest();
            char str[] = new char[16 * 2];
            int k = 0;
            for (int i = 0; i < 16; i++) {
                byte byte0 = temp[i];
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
            result = new String(str);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    
    public String copiarArchivos(File inFile, String id){
        String ruta_destino;
        ruta_destino="src\\fotos_empleados\\"+id;
        try{
            File ff=new File(ruta_destino);
            FileInputStream fis = new FileInputStream(inFile); //inFile -> Archivo a copiar
            FileOutputStream fos = new FileOutputStream(ff); //outFile -> Copia del archivo
            FileChannel inChannel = fis.getChannel(); 
            FileChannel outChannel = fos.getChannel(); 
            inChannel.transferTo(0, inChannel.size(), outChannel); 
            fis.close(); 
            fos.close();
            }catch (IOException ioe) {
            System.err.println("Error al Generar Copia");
            }
        
        
        
        return ruta_destino;
    }
    
    public String buscarAlmacen(String almacenn){
        
        String almacen_bd = null;
        
        //Almacenar almacen
        String almacen_aux;
        StringTokenizer tokens = new StringTokenizer(almacenn,":");
        while(tokens.hasMoreTokens()){
        almacen_aux = tokens.nextToken();
        
        boolean op=almacen_aux.equals("A");
        boolean op2=almacen_aux.equals("B1");
        boolean op3=almacen_aux.equals("B2");
        boolean op4=almacen_aux.equals("B3");
        boolean op5=almacen_aux.equals("C1");
        boolean op6=almacen_aux.equals("C2");
        boolean op7=almacen_aux.equals("D");
        boolean op8=almacen_aux.equals("E");
        boolean op9=almacen_aux.equals("F");
        boolean op10=almacen_aux.equals("G");
        
        if(op==true){
            almacen_bd="deposito_a";
        }if(op2==true){
            almacen_bd="deposito_b1";
        }if(op3==true){
            almacen_bd="deposito_b2";
        }if(op4==true){
            almacen_bd="deposito_b3";
        }if(op5==true){
            almacen_bd="deposito_c1";
        }if(op6==true){
            almacen_bd="deposito_c2";
        }if(op7==true){
            almacen_bd="deposito_d";
        }if(op8==true){
            almacen_bd="deposito_e";
        }if(op9==true){
            almacen_bd="deposito_f";
        }if(op10==true){
            almacen_bd="deposito_g";
        }
        
        }
        return almacen_bd;
    }
    
    public boolean solicitarClave(String clave){
        
        Conexion c=new Conexion();
        
        String clave_md5=getMD5(clave);
        
        c.connect();
        
        int id_ob=c.buscarAdmin(clave_md5);
        boolean op=c.buscarAdmin2(id_ob);
        
        c.close();
        
       
        return op;
    }
    
    public void guardarArchivo() {
        try
        {
         String nombre="";
         JFileChooser file=new JFileChooser();
         //file.showSaveDialog(this);
         File guarda =file.getSelectedFile();

         if(guarda !=null)
         {
          /*guardamos el archivo y le damos el formato directamente,
           * si queremos que se guarde en formato doc lo definimos como .doc*/
           FileWriter  save=new FileWriter(guarda+".txt");
           //save.write(areaDeTexto.getText());
           save.close();
           JOptionPane.showMessageDialog(null,
                "El archivo se a guardado Exitosamente",
                    "Información",JOptionPane.INFORMATION_MESSAGE);
           }
        }
         catch(IOException ex)
         {
          JOptionPane.showMessageDialog(null,
               "Su archivo no se ha guardado",
                  "Advertencia",JOptionPane.WARNING_MESSAGE);
         }
        }
   }
    
