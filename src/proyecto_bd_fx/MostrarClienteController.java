
package proyecto_bd_fx;

import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import static proyecto_bd_fx.Conexion.connect;


public class MostrarClienteController implements Initializable {

    @FXML
    private AnchorPane ventana_mostrarC;
    @FXML
    private ListView<String> lista;
    
    final ObservableList<String> data = FXCollections.observableArrayList(); 
    
    Funciones f=new Funciones();
    Conexion c=new Conexion();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        lista.setItems(data);
        leer();
    }    
    
    @FXML
    private void leer(){
        
        String nombre, cedula;
        int id, cant;
        double precio;
        c.connect();
        ResultSet result = null;
        try {
            PreparedStatement st = connect.prepareStatement("select * from clientes");
            result = st.executeQuery();
            while (result.next()) {
                nombre=result.getString("nombre");
                cedula=result.getString("cedula_rif");
                data.add(new String(nombre+" | C.I: "+cedula));
                
            }
        } catch (SQLException ex) {
            System.err.println("No se ha podido conectar con la tabla\n"+ex.getMessage());
        }
        c.close();
    }
    
    @FXML
    private void volver() throws IOException{
        f.intercambio("Panel.fxml", ventana_mostrarC);
    }
    
}
