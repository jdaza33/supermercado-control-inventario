package proyecto_bd_fx;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

public class Registro2Controller implements Initializable {
    
    @FXML
    private AnchorPane ventana_registro;
    @FXML
    private TextField ciudad;
    @FXML
    private TextField municipio;
    @FXML
    private TextField estado;
    @FXML
    private TextField parroquia;
    @FXML
    private TextField av_calle;
    @FXML
    private TextField sector;
    @FXML
    private TextField res_urb;
    @FXML
    private TextField casa_apt;
    @FXML
    private Button sig;
    
    Funciones f=new Funciones();
    Conexion c=new Conexion();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
      
    }

    @FXML
    private void siguiente() throws IOException{
        f.intercambio("Registro3.fxml", ventana_registro);
    }
    
    @FXML
    private void cancelar() throws IOException{
        f.intercambio("login.fxml", ventana_registro);
    }
    
    @FXML
    private void registrarDireccionE() throws IOException{
        
        int idd;
        String es, ci, pa, pais, mu, se, nro, re, av;
        c.connect();
        idd=c.sumarId();
        idd=idd-1;
        c.close();
        es=estado.getText();
        ci=ciudad.getText();
        pa=parroquia.getText();
        pais="Venezuela";
        mu=municipio.getText();
        se=sector.getText();
        nro=casa_apt.getText();
        av=av_calle.getText();
        re=res_urb.getText();
        
        c.connect();
        c.registrar2(idd, es, ci, mu, pa, av, se, re, nro, pais);
        siguiente();
        c.close();
    }
}
