package proyecto_bd_fx;

import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javax.swing.JOptionPane;

public class IngresarController implements Initializable {

    @FXML
    private TextField nombre;
    @FXML
    private Insets x1;
    @FXML
    private TextField proveedor;
    @FXML
    private TextField cant;
    @FXML
    private TextField precio;
    @FXML
    private TextField fecha;
    @FXML
    private ComboBox<?> almacen;
    @FXML
    private AnchorPane ventana_ingresar;


    Funciones f=new Funciones();
    Conexion c=new Conexion();
    
    String idd, fechaa;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        int id_aux;
        
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        fecha.setText(String.valueOf(dateFormat.format(date)));
        
        
        
    }
    
    @FXML
    private void cancelar() throws IOException{
        f.intercambio("Panel.fxml", ventana_ingresar);
    }
   
    
    @FXML
    private void ingresarAlmacen() throws IOException{
        String nombree, proveedorr, cantidad, precioo, fechaa, almacenn, almacen_destino;
        int idd;
        
        almacenn=(String) almacen.getValue();
        
        almacen_destino=f.buscarAlmacen(almacenn);
        
        c.connect();
        idd=c.idAlmacen(almacen_destino);
        c.close();
        
        fechaa=fecha.getText();
        nombree=nombre.getText();
        proveedorr=proveedor.getText();
        cantidad=cant.getText();
        precioo=precio.getText();
        c.connect();
        c.registrarA(idd, nombree, proveedorr, fechaa, Integer.parseInt(cantidad), Double.parseDouble(precioo), almacen_destino);
        c.close();
        
        JOptionPane.showMessageDialog(null, "Producto Almacenado con Exito");
        //f.intercambio("Panel.fxml", ventana_ingresar);
        
            c.connect();
            String usuario=c.leerAuxiliar();
            c.close();
            
            c.connect();
            c.registros(usuario,"Ingresar Producto",fechaa);
            c.close();
    }
    
}
